﻿using System;
using System.IO;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.AspNetCore.Razor.Language.Extensions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace IVySoft.RazorProducer.BuildUtils
{
    public class GenerateFileTask : Task
    {
        [Required]
        public string TemplateFile { get; set; } = "";
        [Required]
        public string Namespace { get; set; } = "";
        [Required]
        public string InputPath { get; set; } = "";
        [Required]
        public string OutputPath { get; set; } = "";
        [Output]
        public ITaskItem[] GeneratedFiles { get; private set; } = new ITaskItem[1];

        public override bool Execute()
        {
            var fs = RazorProjectFileSystem.Create(InputPath);
            var engine = RazorProjectEngine.Create(
                RazorConfiguration.Default,
                fs,
                (builder) =>
                {
                    InheritsDirective.Register(builder);
                    SectionDirective.Register(builder);
                    FunctionsDirective.Register(builder);

                    builder.ConfigureClass((document, @class) =>
                    {
                        @class.ClassName = Path.GetFileNameWithoutExtension(TemplateFile);
                    });
                    builder.SetNamespace(this.Namespace);
                });
            var item = fs.GetItem(TemplateFile);
            var codeDocument = engine.Process(item);
            var body = codeDocument.GetCSharpDocument();
            var outputFile = Path.Combine(OutputPath, TemplateFile + ".cs");
            var parentFolder = Path.GetDirectoryName(outputFile);
            if(null != parentFolder && 0 != parentFolder.Length)
            {
                Directory.CreateDirectory(parentFolder);
            }
            File.WriteAllText(outputFile, body.GeneratedCode);
            GeneratedFiles[0] = new TaskItem(outputFile);
            return true;
        }
    }
}
