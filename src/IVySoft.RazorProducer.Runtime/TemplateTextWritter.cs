﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace IVySoft.RazorProducer.Runtime
{
    public abstract class TemplateTextWritter
    {
        public TextWriter? OutputStream { get; set; }

        public abstract Task ExecuteAsync();

        protected void WriteLiteral(string literal)
        {
            this.OutputStream?.Write(literal);
        }

        protected void Write(object obj)
        {
            if (obj != null)
            {
                this.OutputStream?.Write(obj.ToString());
            }
        }
        protected void Write()
        {
        }

        protected string _attributeSuffix = string.Empty;
        protected void BeginWriteAttribute(
            string name,
            string prefix,
            int prefixOffset,
            string suffix,
            int suffixOffset,
            int attributeValuesCount)
        {
            _attributeSuffix = suffix;
            this.OutputStream?.Write(prefix);
        }
        protected void WriteAttributeValue(string prefix, int prefixOffset, object value, int valueOffset, int valueLength, bool isLiteral)
        {
            this.OutputStream?.Write(value);
        }
        protected void EndWriteAttribute()
        {
            this.OutputStream?.Write(_attributeSuffix);
        }
    }
}
