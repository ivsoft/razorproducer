﻿using IVySoft.RazorProducer.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject
{
    public abstract class TestTemplate : TemplateTextWritter
    {
        public string? Name { get; set; }
    }
}
