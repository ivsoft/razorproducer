﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestProject
{
    public class TestCase
    {
        [Fact]
        public async Task SimpleTest()
        {
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                var test = new TestFile();
                test.OutputStream = writer;
                test.Name = "World";

                await test.ExecuteAsync();
            }
            Assert.Equal("Hello World!", sb.ToString());
        }
        [Fact]
        public async Task AttrTest()
        {
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                var test = new TestAttributes();
                test.OutputStream = writer;
                test.Name = "World";

                await test.ExecuteAsync();
            }
            Assert.Equal("<Tag attr=\"Const\" attr1=World attr2=\"World\" />", sb.ToString());
        }
    }
}
