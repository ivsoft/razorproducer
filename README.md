# RazorProducer

## Getting started

CSharp file producer based on Razor template

## Usage

Add packages in your project:
```
dotnet add package IVySoft.RazorProducer.BuildUtils
dotnet add package IVySoft.RazorProducer.Runtime
```
After that you can include your templates in csproj:
```
<RazorTemplate Include="TestFile.razor" />
```
RazorProducer generates generator code.
You can use it:
```
var sb = new StringBuilder();
using (var writer = new StringWriter(sb))
{
  var test = new TestFile(); // Your template
  test.OutputStream = writer;
  test.Name = "World"; // Template's properties

  await test.ExecuteAsync();
}
sb.ToString(); // Execute result
```
